package com.robotsidekick.android.subtlereminders;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;

/**
 * Created by alex on 7/24/14.
 */
public class DefaultActivity extends Activity {

    private static int NOTIFY_ID = 100;

    public static void debugNotify(final Context context, final String title, final String message, final Throwable exception) {
        NotificationManager mNotifyMgr =
                (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                .setSmallIcon(android.R.drawable.ic_lock_idle_alarm)
                .setContentTitle(title)
                .setContentText(message)
                .setWhen(0);

        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", "info@robotsidekick.com", null));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "SubtleReminder - Error Log");
        emailIntent.putExtra(Intent.EXTRA_TEXT, message + "\n" + message + "\n" + getString(new StringBuilder(), exception));

        builder.addAction(android.R.drawable.ic_dialog_email, "E-Mail Log", PendingIntent.getActivity(context, 0, emailIntent, 0));

        mNotifyMgr.notify(NOTIFY_ID, builder.build());
    }

    private static String getString(final StringBuilder string, final Throwable exception) {
        if (exception != null) {
            for (StackTraceElement element : exception.getStackTrace()) {
                string.append(element.getFileName())
                        .append("\t")
                        .append(element.getClassName())
                        .append(".")
                        .append(element.getMethodName()).append("()")
                        .append("\t")
                        .append(element.getLineNumber())
                        .append("\n");
            }
        }

        if (exception != null && exception.getCause() != null) {
            string.append("\n");
            return getString(string, exception.getCause());
        } else {
            return string.toString();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        finish();
    }
}
