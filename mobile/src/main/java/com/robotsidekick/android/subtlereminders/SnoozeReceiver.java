package com.robotsidekick.android.subtlereminders;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.format.DateUtils;
import android.text.format.Time;
import android.util.Log;

import java.io.Serializable;

/**
 * Created by alex on 7/24/14.
 */
public class SnoozeReceiver extends BroadcastReceiver {

    private static final String TAG = SnoozeReceiver.class.getSimpleName();

    public void onReceive(Context context, Intent intent) {

        SpecialDate date = null;
        if (intent != null) {
            Serializable serializable = intent.getSerializableExtra(SpecialDateReceiver.EXTRA_SPECIAL_DATE);
            if (serializable != null && serializable instanceof SpecialDate) {

                date = (SpecialDate) serializable;

                long currentTimeMillis = System.currentTimeMillis();
                long nextUpdateTimeMillis = currentTimeMillis + (DateUtils.MINUTE_IN_MILLIS * 60);
                Time nextUpdateTime = new Time();
                nextUpdateTime.set(nextUpdateTimeMillis);

                AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
                alarmManager.set(AlarmManager.RTC, nextUpdateTimeMillis, SpecialDateReceiver.createIntent(context, date));
            }
        }

        if (date != null) {
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.cancel(date.getNotificationId());
        }
    }
}
