package com.robotsidekick.android.subtlereminders;

import android.app.AlarmManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import java.util.Calendar;

/**
 * Created by alex on 7/23/14.
 */
public class BootBroadcastReceiver extends BroadcastReceiver {

    private static final String TAG = BootBroadcastReceiver.class.getCanonicalName();

    @Override
    public void onReceive(final Context context, final Intent intent) {

        // DefaultActivity.debugNotify(context, "Received Boot Broadcast", "", null);

        SpecialDate.startAlarmManager(context);
    }
}
