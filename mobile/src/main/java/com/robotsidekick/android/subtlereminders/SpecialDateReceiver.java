package com.robotsidekick.android.subtlereminders;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by alex on 7/23/14.
 */
public class SpecialDateReceiver extends BroadcastReceiver {

    public static final String EXTRA_SPECIAL_DATE = "EXTRA_SPECIAL_DATE";
    private static final String TAG = SpecialDateReceiver.class.getSimpleName();
    private static int notificationId = 55;

    public static PendingIntent createIntent(final Context context) {
        return createIntent(context, null);
    }

    public static PendingIntent createIntent(final Context context, final SpecialDate date) {
        int requestCode;
        Intent specialDateReceiver = new Intent(context, SpecialDateReceiver.class);
        if (date != null) {
            specialDateReceiver.putExtra(EXTRA_SPECIAL_DATE, date);
            if (date.hasNotificationId()) {
                requestCode = date.getNotificationId() * 10;
            } else {
                requestCode = notificationId++;
            }
        } else {
            requestCode = notificationId++;
        }
        return PendingIntent.getBroadcast(context, requestCode, specialDateReceiver, PendingIntent.FLAG_CANCEL_CURRENT);
    }

    @Override
    public void onReceive(final Context context, final Intent intent) {

        try {

            Bundle extras = intent.getExtras();

            List<SpecialDate> notificationReadyDates = new ArrayList<SpecialDate>();

            if (intent != null && intent.hasExtra(EXTRA_SPECIAL_DATE)) {

                Serializable date = intent.getSerializableExtra(EXTRA_SPECIAL_DATE);
                if (date instanceof SpecialDate) {
                    notificationReadyDates.add((SpecialDate) date);
                }

            } else {

                notificationReadyDates = SpecialDate.getSpecialDatesFromContactsForToday(context);
                SpecialDate.updateDebugPrefsCheckedForSpecialDates(context);

            }

            NotificationManager mNotificationManager =
                    (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            for (SpecialDate date: notificationReadyDates) {
                if (!date.hasNotificationId()) {
                    date.setNotificationId(context, notificationId++);
                }
                mNotificationManager.notify(date.getNotificationId(), date.createNotification(context));
            }

        } catch (Throwable t) {
            Log.e(TAG, "Subtle Reminders failed : (", t);
//            DefaultActivity.debugNotify(context, "Failed to notify user", "", t);
        } finally {
//            PendingIntent ret = PendingIntent.getBroadcast(context, 0,
//                    new Intent(context, SpecialDateReceiver.class),
//                    PendingIntent.FLAG_NO_CREATE);
//            DefaultActivity.debugNotify(context, "Notification Id = " + notificationId, ret == null ? "no intent scheduled" : "intent scheduled: " + ret.getCreatorPackage(), null);
        }
    }

    private String getInCondition(final Set<String> contactIds) {
        StringBuilder buf = new StringBuilder();
        String sep = ",";
        for (String id : contactIds) {
            buf.append(sep).append('"').append(id).append('"');
        }
        return buf.toString();
    }
}
