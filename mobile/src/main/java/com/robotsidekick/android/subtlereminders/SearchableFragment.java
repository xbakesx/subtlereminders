package com.robotsidekick.android.subtlereminders;

/**
 * Created by alex on 1/5/15.
 */
public interface SearchableFragment {
    void onSearch(final String search);
}
