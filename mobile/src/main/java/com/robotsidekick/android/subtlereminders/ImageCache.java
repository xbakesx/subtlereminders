package com.robotsidekick.android.subtlereminders;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.util.LruCache;
import android.util.Log;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * Created by alex on 1/5/15.
 */
public class ImageCache {

    private static final String TAG = ImageCache.class.getCanonicalName();
    private static ImageCache cache = new ImageCache();

    public static Bitmap getBitmap(final Context context, final String uri) {
        return getBitmap(context, uri, uri);
    }

    public static Bitmap getBitmap(final Context context, final String key, final String uri) {
        return getBitmap(context, key, uri, null);
    }

    public static Bitmap getBitmap(final Context context, final String uri, final BitmapTransformer loader) {
        return getBitmap(context, uri, uri, loader);
    }

    public static Bitmap getBitmap(final Context context, final String key, final String uri, final BitmapTransformer loader) {
        Bitmap ret = null;
        if (uri != null && uri.trim().length() > 0) {
            if ((ret = cache.imageCache.get(key)) == null && uri != null && uri.trim().length() > 0) {
                ret = createBitmapFromContactUri(context, uri);
                if (loader != null) {
                    ret = loader.transform(context, ret);
                }
                cache.imageCache.put(key, ret);
            }
        }
        return ret;
    }

    public static Future<Bitmap> getBitmapLater(final Context context, final String key, final String uri, final BitmapTransformer transformer, final BitmapLoadedCallback callback) {

        Future<Bitmap> ret = null;
        if (uri != null && uri.trim().length() > 0) {

            Bitmap cachedBitmap = cache.imageCache.get(key);

            if (cachedBitmap == null && cache.backgroundTasks.containsKey(key)) {

                // If we've already started working on this before, but it hasn't completed...
                ret = cache.backgroundTasks.get(key);

            } else if (cachedBitmap == null || cachedBitmap.isRecycled()) {

                cachedBitmap = null;

                // If the image isn't cached (or is but it's been recycled), transform it up in a background thread

                ret = new FutureBitmap(new AsyncTask<Object, Void, Bitmap>() {

                    @Override
                    protected Bitmap doInBackground(final Object... params) {

                        Bitmap ret = createBitmapFromContactUri(context, uri);
                        if (transformer != null) {
                            ret = transformer.transform(context, ret);
                        }
                        cache.imageCache.put(key, ret);

                        return ret;
                    }

                    @Override
                    protected void onPostExecute(final Bitmap bitmap) {
                        if (callback != null) {
                            callback.complete(bitmap);
                        }
                    }
                });
                cache.backgroundTasks.put(key, ret);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    ((FutureBitmap) ret).task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                } else {
                    ((FutureBitmap) ret).task.execute();
                }
            }

            // If it is cached, pretend it's being loaded up
            if (cachedBitmap != null) {
                ret = new FutureBitmap(cachedBitmap);
            }
        }

        return ret;
    }

    public static Bitmap createBitmapFromContactUri(final Context context, final String photoUriString) {
        if (context == null || photoUriString == null || photoUriString.trim().length() == 0) {
            return null;
        }
        Bitmap photo = null;
        try {
            Uri uri = Uri.parse(photoUriString);
            InputStream input = context.getContentResolver().openInputStream(uri);
            if (input != null) {
                photo = BitmapFactory.decodeStream(input);
            }
        } catch (final FileNotFoundException e) {
            Log.w(TAG, "Couldn't find contact image: " + photoUriString, e);
        } catch (final OutOfMemoryError e) {
            Log.e(TAG, "Failed to create image (out of memory): " + photoUriString, e);
            StringBuffer buf = new StringBuffer();
            for (String key : ImageCache.cache.backgroundTasks.keySet()) {
                buf.append("\n" + key);
            }
            Log.wtf(TAG, buf.toString());
        } catch (final Throwable t) {
            Log.w(TAG, "Couldn't find contact image: " + photoUriString, t);
        }
        return photo;
    }

    private final LruCache<String, Bitmap> imageCache;
    private final Map<String, Future<Bitmap>> backgroundTasks;

    private ImageCache() {
        int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
        imageCache = new LruCache<String, Bitmap>(maxMemory / 8) {
            @Override
            protected int sizeOf(final String key, final Bitmap bitmap) {
                // The cache size will be measured in kilobytes rather than
                // number of items.
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1) {
                    return bitmap.getByteCount() / 1024;
                } else {
                    return (bitmap.getRowBytes() * bitmap.getHeight()) / 1024;
                }
            }

            @Override
            protected void entryRemoved(final boolean evicted, final String key, final Bitmap oldValue, final Bitmap newValue) {
                super.entryRemoved(evicted, key, oldValue, newValue);
                if (backgroundTasks.containsKey(key)) {
                    backgroundTasks.remove(key);
                }
            }
        };
        backgroundTasks = new HashMap<>();
    }

    public static interface BitmapTransformer {
        Bitmap transform(final Context context, final Bitmap bitmap);
    }

    public static interface BitmapLoadedCallback {
        void complete(final Bitmap bitmap);
    }

    public static final class FutureBitmap implements Future<Bitmap> {

        private final AsyncTask<?,?,Bitmap> task;
        private Bitmap bitmap;

        private FutureBitmap(final AsyncTask task) {
            this.task = task;
            bitmap = null;
        }

        private FutureBitmap(final Bitmap bitmap) {
            task = null;
            this.bitmap = bitmap;
        }

        @Override
        public boolean cancel(final boolean mayInterruptIfRunning) {
            if (task != null) {
                return task.cancel(mayInterruptIfRunning);
            }
            return false;
        }

        @Override
        public boolean isCancelled() {
            return task == null ? false : task.isCancelled();
        }

        @Override
        public boolean isDone() {
            return bitmap != null;
        }

        @Override
        public Bitmap get() throws InterruptedException, ExecutionException {
            return task == null ? bitmap : task.get();
        }

        @Override
        public Bitmap get(final long timeout, final TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
            if (task == null) {
                return bitmap;
            } else {
                return task.get(timeout, unit);
            }
        }
    }
}
