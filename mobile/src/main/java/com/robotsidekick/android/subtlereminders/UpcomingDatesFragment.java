package com.robotsidekick.android.subtlereminders;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Created by alex on 12/30/14.
 */
public class UpcomingDatesFragment extends Fragment implements SearchableFragment {

    private final ThreadPoolExecutor executor = new ThreadPoolExecutor(1, 4, 1, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>());
    private UpcomingDatesAdapter adapter;

    @Override
    public View onCreateView(final LayoutInflater inflater,
                             final ViewGroup container,
                             final Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_upcoming_dates, container, false);
        ListView list = (ListView) rootView.findViewById(R.id.fragment_upcoming_dates_listview);
        adapter = new UpcomingDatesAdapter(getActivity(), list, executor);
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(final AdapterView<?> parent, final View view, final int position, final long id) {

                SpecialDate specialDate = (SpecialDate) parent.getAdapter().getItem(position);

                Intent intent = new Intent(Intent.ACTION_VIEW);
                Uri uri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_URI, String.valueOf(specialDate.getId()));
                intent.setData(uri);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                startActivity(intent);
            }
        });
        list.setDividerHeight(0);
        return rootView;
    }

    @Override
    public void onSearch(final String search) {
        if (adapter != null) {
            adapter.getFilter().filter(search);
        }
    }

    public static class UpcomingDatesAdapter extends ArrayAdapter<SpecialDate> implements Filterable {

        private final SpecialDateComparator comparator;
        private final Filter nameFilter;
        private List<SpecialDate> allSpecialDates;

        public UpcomingDatesAdapter(final Activity activity, final ListView listView, final ThreadPoolExecutor executor) {
            super(activity, android.R.layout.simple_list_item_1, new ArrayList<SpecialDate>());
            allSpecialDates = new ArrayList<SpecialDate>();

            executor.execute(new SpecialDateFetcher(activity, this));
            comparator = new SpecialDateComparator();
            nameFilter = new Filter() {
                @Override
                protected FilterResults performFiltering(final CharSequence constraint) {
                    FilterResults results = new FilterResults();

                    List<SpecialDate> filteredList;
                    if (constraint == null || constraint.length() == 0) {
                        filteredList = new ArrayList<>();
                        filteredList.addAll(allSpecialDates);
                    } else {
                        filteredList = new ArrayList<>();
                        for (final SpecialDate date : allSpecialDates) {
                            if (date.getName().toLowerCase().contains(constraint)) {
                                filteredList.add(date);
                            }
                        }
                    }

                    results.count = filteredList.size();
                    results.values = filteredList;

                    return results;
                }

                @Override
                protected void publishResults(final CharSequence constraint, final FilterResults results) {
                    clear();
                    for (SpecialDate date : (List<SpecialDate>) results.values) {
                        UpcomingDatesAdapter.super.add(date);
                    }
                    UpcomingDatesAdapter.this.sort(comparator);
                    notifyDataSetChanged();
                }
            };
        }


        @Override
        public View getView(final int position, final View convertView, final ViewGroup parent) {

            final SpecialDate specialDate = getItem(position);
            final View view;

            ViewHolder holder;
            if (convertView == null) {
                view = LayoutInflater.from(getContext()).inflate(R.layout.listview_row_specialdate, parent, false);
                holder = new ViewHolder(
                        ((ImageView) view.findViewById(R.id.listview_row_specialdate_photo)),
                        ((TextView) view.findViewById(R.id.listview_row_specialdate_title)),
                        ((TextView) view.findViewById(R.id.listview_row_specialdate_date)),
                        ((TextView) view.findViewById(R.id.listview_row_specialdate_subtitle))
                );
                view.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
                view = convertView;
            }

            holder.title.setText(specialDate.getName());
            SpecialDateType representativeType = specialDate.getRepresentativeType();
            holder.date.setText(representativeType.getDateAsString());
            String subTitle;
            if (representativeType.hasAge()) {
                if (representativeType.isBirthday()) {
                    subTitle = getContext().getString(R.string.listview_row_specialdate_subtitle_birthday, representativeType.getUpcomingAge());
                } else if (representativeType.isAnniversary()) {
                    try {
                        final int age = representativeType.getUpcomingAgeAsInt();
                        subTitle = getContext().getResources().getQuantityString(R.plurals.listview_row_specialdate_subtitle_anniversary, age, age);
                    } catch (NumberFormatException ex) {
                        subTitle = getContext().getString(R.string.listview_row_specialdate_subtitle_anniversary, representativeType.getUpcomingAge());
                    }
                } else {
                    subTitle = getContext().getString(R.string.listview_row_specialdate_subtitle_other, representativeType.getName(), representativeType.getUpcomingAge());
                }
            } else {
                subTitle = representativeType.getName();
            }

            holder.subTitle.setText(subTitle);
            int subTitleIcon = -1;
            try {
                Bitmap thumbnail = specialDate.getThumbnailBitmap(getContext());
                if (thumbnail != null) {
                    holder.thumbnail.setImageDrawable(new RoundedCornerBitmapDrawable(getContext(), thumbnail));
                    if (representativeType.isBirthday()) {
                        subTitleIcon = R.drawable.ic_cake;
                    } else if (representativeType.isAnniversary()) {
                        subTitleIcon = R.drawable.ic_ring;
                    }
                } else {
                    int thumbnailIcon;
                    if (representativeType.isBirthday()) {
                        thumbnailIcon = R.drawable.contact_icon_birthday;
                    } else if (representativeType.isAnniversary()) {
                        thumbnailIcon = R.drawable.contact_icon_anniversary;
                    } else {
                        thumbnailIcon = R.drawable.contact_icon_other;
                    }
                    Drawable drawable = getContext().getResources().getDrawable(thumbnailIcon);
                    drawable.setLevel((position % 5) * 10); // Cycles through the color palette

                    holder.thumbnail.setImageDrawable(new RoundedCornerBitmapDrawable(getContext(), drawable, 12));
                }
            } catch (OutOfMemoryError e) {
                // Memory issues...
            } catch (NullPointerException ex) {
                // Memory issues passed into LruCache...
            }
            if (subTitleIcon != -1) {
                holder.subTitle.setCompoundDrawablesWithIntrinsicBounds(0, 0, subTitleIcon, 0);
            }

            if (specialDate.hasPhotoUri()) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    view.setBackground(new FancyBackgroundDrawable(getContext(), specialDate.getPhotoUri(), view));
                }
                holder.title.setTextColor(getContext().getResources().getColor(android.R.color.white));
                holder.title.setShadowLayer(5f, 0f, 0f, getContext().getResources().getColor(android.R.color.black));
                holder.subTitle.setTextColor(getContext().getResources().getColor(android.R.color.white));
                holder.subTitle.setShadowLayer(5f, 0f, 0f, getContext().getResources().getColor(android.R.color.black));
                holder.date.setTextColor(getContext().getResources().getColor(android.R.color.white));
                holder.date.setShadowLayer(5f, 0f, 0f, getContext().getResources().getColor(android.R.color.black));
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    view.setBackground((Drawable) null);
                }
                view.setBackgroundColor(getContext().getResources().getColor(android.R.color.white));
                holder.title.setTextColor(getContext().getResources().getColor(android.R.color.black));
                holder.title.setShadowLayer(5f, 0f, 0f, getContext().getResources().getColor(android.R.color.white));
                holder.subTitle.setTextColor(getContext().getResources().getColor(android.R.color.black));
                holder.subTitle.setShadowLayer(5f, 0f, 0f, getContext().getResources().getColor(android.R.color.white));
                holder.date.setTextColor(getContext().getResources().getColor(android.R.color.black));
                holder.date.setShadowLayer(5f, 0f, 0f, getContext().getResources().getColor(android.R.color.white));
            }

            return view;
        }

        @Override
        public void add(final SpecialDate object) {
            super.add(object);
            allSpecialDates.add(object);
            sort(comparator);
        }

        @Override
        public Filter getFilter() {
            return nameFilter;
        }

    }

    public static class SpecialDateFetcher implements Runnable {

        private final Activity activity;
        private final ArrayAdapter adapter;

        public SpecialDateFetcher(final Activity activity, final ArrayAdapter adapter) {
            this.activity = activity;
            this.adapter = adapter;
        }

        @Override
        public void run() {
            final List<SpecialDate> specialDates = SpecialDate.getSpecialDatesFromContactsForUpcomingMonths(activity);
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    synchronized (adapter) {
                        adapter.clear();
                        for (SpecialDate specialDate : specialDates) {
                            adapter.add(specialDate);
                        }
                        adapter.notifyDataSetChanged();
                    }
                }
            });
        }
    }

    public static class ViewHolder {
        private final ImageView thumbnail;
        private final TextView title;
        private final TextView date;
        private final TextView subTitle;

        public ViewHolder(ImageView thumbnail, TextView title, TextView date, TextView subTitle) {
            this.thumbnail = thumbnail;
            this.title = title;
            this.date = date;
            this.subTitle = subTitle;
        }
    }

}
