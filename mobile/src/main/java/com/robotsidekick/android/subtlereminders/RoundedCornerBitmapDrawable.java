package com.robotsidekick.android.subtlereminders;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.TypedValue;

/**
 * Created by alex on 1/5/15.
 */
public class RoundedCornerBitmapDrawable extends BitmapDrawable {

    public static Bitmap drawableToBitmap (final Drawable drawable) {
        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable)drawable).getBitmap();
        }

        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }

    private final Paint paint;
    private final RectF rect;

    private float radius;

    public RoundedCornerBitmapDrawable(final Context context, final Bitmap bitmap) {
        this(context, bitmap, 4);
    }

    public RoundedCornerBitmapDrawable(final Context context, final Drawable drawable, final int radius) {
        this(context, drawableToBitmap(drawable), radius);
    }

    public RoundedCornerBitmapDrawable(final Context context, final Bitmap bitmap, final int radius) {
        super(context.getResources(), bitmap);
        setRadius(context, radius);

        BitmapShader shader = new BitmapShader(bitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);

        paint = new Paint();
        paint.setAntiAlias(true);
        paint.setShader(shader);

        rect = new RectF(0.0f, 0.0f, bitmap.getWidth(), bitmap.getHeight());
    }

    @Override
    public void draw(final Canvas canvas) {
        // rect contains the bounds of the shape
        // radius is the radius in pixels of the rounded corners
        // paint contains the shader that will texture the shape
        canvas.drawRoundRect(rect, radius, radius, paint);
    }

    public void setRadius(final Context context, final int radiusInDp) {
        if (context != null) {
            this.radius = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, radiusInDp, context.getResources().getDisplayMetrics());
        } else {
            this.radius =  0f;
        }
    }
}
