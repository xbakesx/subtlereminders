package com.robotsidekick.android.subtlereminders;

import java.util.Comparator;

/**
 * Created by alex on 12/31/14.
 */
public class SpecialDateComparator implements Comparator<SpecialDate> {

    @Override
    public int compare(final SpecialDate lhs, final SpecialDate rhs) {
        SpecialDateType lhType;
        SpecialDateType rhType;
        if (lhs == null || (lhType = lhs.getRepresentativeType()) == null || (lhType.getDateForComparator() == null)) {
            return -1;
        }
        if (rhs == null || (rhType = rhs.getRepresentativeType()) == null || (rhType.getDateForComparator() == null)) {
            return 1;
        }

        return lhType.getDateForComparator().compareTo(rhType.getDateForComparator());
    }

}
