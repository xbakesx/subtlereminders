package com.robotsidekick.android.subtlereminders;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.provider.ContactsContract;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by alex on 7/24/14.
 */
public class SpecialDate implements Serializable {

    private static final String TAG = SpecialDate.class.getSimpleName();
    private static final String DEBUG_SHARED_PREFS = "DEBUG_SHARED_PREFS";

    public static Calendar getNow() {
        Calendar now = GregorianCalendar.getInstance();
        now.set(Calendar.HOUR_OF_DAY, 0);
        now.set(Calendar.MINUTE, 0);
        now.set(Calendar.SECOND, 0);
        now.set(Calendar.MILLISECOND, 0);
        return now;
    }

    public static void updateDebugPrefsCheckedForSpecialDates(final Context context) {
        SharedPreferences prefs = context.getSharedPreferences(DEBUG_SHARED_PREFS, Context.MODE_PRIVATE);
        prefs.edit().putLong("PREF_CHECK_SPECIAL_DATES", System.currentTimeMillis()).commit();
    }

    public static String getDebugPrefsLastCheckedForSpecialDates(final Context context) {
        SharedPreferences prefs = context.getSharedPreferences(DEBUG_SHARED_PREFS, Context.MODE_PRIVATE);
        long lastChecked = prefs.getLong("PREF_CHECK_SPECIAL_DATES", 0);
        if (lastChecked == 0) {
            return "Never";
        } else {
            return SimpleDateFormat.getDateTimeInstance().format(new Date(lastChecked));
        }
    }

    public static void updateDebugPrefsStartedAlarmManager(final Context context) {
        SharedPreferences prefs = context.getSharedPreferences(DEBUG_SHARED_PREFS, Context.MODE_PRIVATE);
        prefs.edit().putLong("PREF_ALARM_STARTED", System.currentTimeMillis()).commit();
    }

    public static String getDebugPrefsLastStartedAlarmManager(final Context context) {
        SharedPreferences prefs = context.getSharedPreferences(DEBUG_SHARED_PREFS, Context.MODE_PRIVATE);
        long lastChecked = prefs.getLong("PREF_ALARM_STARTED", 0);
        if (lastChecked == 0) {
            return "Never";
        } else {
            return SimpleDateFormat.getDateTimeInstance().format(new Date(lastChecked));
        }
    }

    public static void startAlarmManager(final Context context) {

        try {
            AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

            Calendar calendar = SpecialDate.getNow();
            calendar.set(Calendar.HOUR_OF_DAY, 5);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);

            final PendingIntent pendingIntent = SpecialDateReceiver.createIntent(context);

            alarmManager.cancel(pendingIntent);
            alarmManager.setInexactRepeating(AlarmManager.RTC,
                    calendar.getTimeInMillis(),
                    AlarmManager.INTERVAL_DAY,
                    pendingIntent);

            updateDebugPrefsStartedAlarmManager(context);
        } catch (Throwable t) {
            Log.e(TAG, "Failed to start Alarm", t);
        }

    }

    public static List<SpecialDate> getSpecialDatesFromContactsForToday(final Context context) {
        final List<SpecialDate> ret = new ArrayList<SpecialDate>();
        final Map<String, List<SpecialDate>> dates = getSpecialDatesFromContacts(context, new HashMap<String, List<SpecialDate>>(), SpecialDate.getNow(), true);

        for (String contactId : dates.keySet()) {
            SpecialDate date = SpecialDate.mergeSpecialDates(dates.get(contactId));
            ret.add(date);
        }

        return ret;
    }

    public static List<SpecialDate> getSpecialDatesFromContactsForThisMonth(final Context context) {
        final List<SpecialDate> ret = new ArrayList<SpecialDate>();
        final Map<String, List<SpecialDate>> dates = getSpecialDatesFromContacts(context, new HashMap<String, List<SpecialDate>>(), SpecialDate.getNow(), false);

        for (String contactId : dates.keySet()) {
            SpecialDate date = SpecialDate.mergeSpecialDates(dates.get(contactId));
            ret.add(date);
        }

        return ret;
    }

    public static List<SpecialDate> getSpecialDatesFromContactsForUpcomingMonths(final Context context) {
        final List<SpecialDate> ret = new ArrayList<SpecialDate>();
        Calendar now = getNow();

        Map<String, List<SpecialDate>> dates = new HashMap<String, List<SpecialDate>>();

        for (int i = 0; i < 12; ++i) {
            dates = getSpecialDatesFromContacts(context, dates, now, false);

            now.add(Calendar.MONTH, 1);
        }

        // Now combine them by contact id
        for (String contactId : dates.keySet()) {
            ret.addAll(dates.get(contactId));
//            SpecialDate date = SpecialDate.mergeSpecialDates(dates.get(contactId));
//            ret.add(date);
        }

        return ret;
    }

    public static Map<String, List<SpecialDate>> getSpecialDatesFromContacts(final Context context, final Map<String, List<SpecialDate>> dates, final Calendar cal, final boolean isSingleDay) {

        final Set<String> contactIds = new HashSet<String>();

        /**
         * Get all the specific contact information out by the date.
         */
        String[] proj;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            // TODO: I can retrieve photos via the PHOTO_ID field, but that seems like a lot of work
            // to support API < 11
            proj = new String[]{
                    ContactsContract.Data.CONTACT_ID,
                    ContactsContract.Data.DISPLAY_NAME_PRIMARY,
                    ContactsContract.Data.DISPLAY_NAME,
                    ContactsContract.Data.DATA1, // Date
                    ContactsContract.Data.DATA2, // Type 0-3 Custom, Anniversary, Other, Birthday
                    ContactsContract.Data.DATA3, // Name for field
                    ContactsContract.Data.PHOTO_THUMBNAIL_URI,
                    ContactsContract.Data.PHOTO_URI
            };
        } else {
            proj = new String[]{
                    ContactsContract.Data.CONTACT_ID,
                    ContactsContract.Data.DISPLAY_NAME_PRIMARY,
                    ContactsContract.Data.DISPLAY_NAME,
                    ContactsContract.Data.DATA1, // Date
                    ContactsContract.Data.DATA2, // Type 0-3 Custom, Anniversary, Other, Birthday
                    ContactsContract.Data.DATA3 // Name for field
            };
        }

        ContentResolver resolver = context.getContentResolver();
        String selection = ContactsContract.Data.MIMETYPE + " = ? AND " + ContactsContract.Data.DATA1 + " LIKE ?";

        String[] args;
        if (isSingleDay) {
            args = new String[] {
                    ContactsContract.CommonDataKinds.Event.CONTENT_ITEM_TYPE,
                    "%-" + zeroPad(cal.get(Calendar.MONTH) + 1) + "-" + zeroPad(cal.get(Calendar.DAY_OF_MONTH))
            };
        } else {
            args = new String[] {
                    ContactsContract.CommonDataKinds.Event.CONTENT_ITEM_TYPE,
                    "%-" + zeroPad(cal.get(Calendar.MONTH) + 1) + "-%"
            };
        }
        Cursor dataCursor = resolver.query(ContactsContract.Data.CONTENT_URI, proj, selection, args, null);
        while (dataCursor.moveToNext()) {
            String contactId = dataCursor.getString(dataCursor.getColumnIndex(ContactsContract.Data.CONTACT_ID));
            contactIds.add(contactId);

            SpecialDate date;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                date = new SpecialDate(
                        context,
                        contactId,
                        new String[] {
                                dataCursor.getString(dataCursor.getColumnIndex(ContactsContract.Data.DISPLAY_NAME_PRIMARY)),
                                dataCursor.getString(dataCursor.getColumnIndex(ContactsContract.Data.DISPLAY_NAME)),
                        },
                        dataCursor.getString(dataCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)),
                        dataCursor.getString(dataCursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.ADDRESS)),
                        dataCursor.getString(dataCursor.getColumnIndex(ContactsContract.Data.DATA1)),
                        dataCursor.getString(dataCursor.getColumnIndex(ContactsContract.Data.DATA2)),
                        dataCursor.getString(dataCursor.getColumnIndex(ContactsContract.Data.DATA3)),
                        dataCursor.getString(dataCursor.getColumnIndex(ContactsContract.Data.PHOTO_THUMBNAIL_URI)),
                        dataCursor.getString(dataCursor.getColumnIndex(ContactsContract.Data.PHOTO_URI))
                );
            } else {
                date = new SpecialDate(
                        context,
                        contactId,
                        new String[] {
                                dataCursor.getString(dataCursor.getColumnIndex(ContactsContract.Data.DISPLAY_NAME_PRIMARY)),
                                dataCursor.getString(dataCursor.getColumnIndex(ContactsContract.Data.DISPLAY_NAME)),
                        },
                        dataCursor.getString(dataCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)),
                        dataCursor.getString(dataCursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.ADDRESS)),
                        dataCursor.getString(dataCursor.getColumnIndex(ContactsContract.Data.DATA1)),
                        dataCursor.getString(dataCursor.getColumnIndex(ContactsContract.Data.DATA2)),
                        dataCursor.getString(dataCursor.getColumnIndex(ContactsContract.Data.DATA3)),
                        null,
                        null
                );
            }
            List<SpecialDate> list;
            if (dates.containsKey(contactId)) {
                list = dates.get(contactId);
                list.add(date);
                dates.put(contactId, list);
            } else {
                list = new ArrayList<SpecialDate>();
                list.add(date);
                dates.put(contactId, list);
            }
        }
        dataCursor.close();

        /**
         * Get the phone and email addresses for the contact ids from above.
         */
        // TODO: the "in" clause should be done in sql, not in java
        dataCursor = resolver.query(
                ContactsContract.Data.CONTENT_URI,
                new String[] { ContactsContract.Data.CONTACT_ID, ContactsContract.Data.DISPLAY_NAME_PRIMARY, ContactsContract.Data.DATA1, ContactsContract.Data.MIMETYPE },
                //                    ContactsContract.Data.CONTACT_ID + " in (?) AND (" + ContactsContract.Data.MIMETYPE + "=? OR " + ContactsContract.Data.MIMETYPE + "=?)",
                ContactsContract.Data.MIMETYPE + "=? OR " + ContactsContract.Data.MIMETYPE + "=?",
                //                    new String[]{ getInCondition(contactIds), ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE},
                new String[]{ ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE},
                ContactsContract.Data.CONTACT_ID);
        while (dataCursor.moveToNext()) {
            String contactId = dataCursor.getString(dataCursor.getColumnIndex(ContactsContract.Data.CONTACT_ID));
            if (contactIds.contains(contactId)) {
                for (SpecialDate date : dates.get(contactId)) {
                    String data = dataCursor.getString(dataCursor.getColumnIndex(ContactsContract.Data.DATA1));
                    if (dataCursor.getString(dataCursor.getColumnIndex(ContactsContract.Data.MIMETYPE)).equals(ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE)) {
                        date.setEmail(data);
                    } else {
                        date.setPhone(data);
                    }
                }
            }
        }
        dataCursor.close();

        return dates;
    }

    public static SpecialDate mergeSpecialDates(final List<SpecialDate> dates) {
        if (dates != null && dates.size() == 1) {
            return dates.get(0);
        }
        SpecialDate merged = new SpecialDate();
        merged.id = "";
        merged.name = "";
        merged.email = "";
        merged.phone = "";
        merged.thumbnailUri = "";
        merged.photoUri = "";
        merged.types = new ArrayList<SpecialDateType>();

        if (dates != null) {
            for (SpecialDate date : dates) {
                if (merged.id.trim().length() == 0 && date.id != null) {
                    merged.id = date.id;
                }
                if (merged.name.trim().length() == 0 && date.name != null) {
                    merged.name = date.name;
                }
                if (merged.email.trim().length() == 0 && merged.email != null) {
                    merged.email = date.email;
                }
                if (merged.phone.trim().length() == 0 && merged.phone != null) {
                    merged.phone = date.phone;
                }
                if (merged.thumbnailUri.trim().length() == 0 && date.thumbnailUri != null) {
                    merged.thumbnailUri = date.thumbnailUri;
                }
                if (merged.photoUri.trim().length() == 0 && date.photoUri != null) {
                    merged.photoUri = date.photoUri;
                }
                for (SpecialDateType type : date.types) {
                    merged.types.add(type);
                }
            }
        }

        return merged;
    }

    private String id;
    private String name;
    private String email;
    private String phone;
    private String thumbnailUri;
    private String photoUri;
    private int notificationId;

    private List<SpecialDateType> types;

    private SpecialDate() {
        // constructor for merging multiple special dates
        notificationId = -1;
    }

    public SpecialDate(final String id, final String name, final String email, final String phone, final SpecialDateType type, final String thumbnailUri, final String photoUri) {
        this();

        this.id = id;
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.types = Collections.singletonList(type);
        this.thumbnailUri = thumbnailUri;
        this.photoUri = photoUri;
    }

    public SpecialDate(final Context context, final String iId, final String[] iNames, final String iEmail, final String iPhone, final String iDate, final String iType, final String iTypeName, final String iThumbnail, final String iPhoto) {
        this();

        id = iId;
        name = context.getString(R.string.date_name_default);
        for (String iName : iNames) {
            if (iName != null && iName.trim().length() > 0) {
                name = iName;
                break;
            }
        }

        if (iEmail != null && iEmail.trim().length() > 0) {
            email = iEmail;
        } else {
            email = "";
        }

        if (iPhone != null && iPhone.trim().length() > 0) {
            phone = iPhone;
        } else {
            phone = "";
        }

        types = Collections.singletonList(new SpecialDateType(context, iType, iTypeName, iDate));

        thumbnailUri = iThumbnail;
        photoUri = iPhoto;
    }

    public SpecialDate copy() {
        SpecialDate copy = new SpecialDate();
        copy.id = this.id;
        copy.name = this.name;
        copy.email = this.email;
        copy.phone = this.phone;
        copy.thumbnailUri = this.thumbnailUri;
        copy.notificationId = this.notificationId;
        copy.types = new ArrayList<SpecialDateType>();
        for (SpecialDateType type : this.types) {
            copy.types.add(type.copy());
        }
        return copy;
    }

    public String toString() {
        return name + "\t" + types.get(0) + " (" + types.size() + ")";
    }

    public void setNotificationId(final Context context, final int iNotificationId) {
        if (hasNotificationId()) {
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.cancel(notificationId);
        }
        notificationId = iNotificationId;
    }

    public int getNotificationId() {
        return notificationId;
    }

    public String getId() {
        return id;
    }

    public boolean hasNotificationId() {
        return notificationId != -1;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public String getThumbnailUri() {
        return thumbnailUri;
    }

    public String getPhotoUri() {
        return photoUri;
    }

    public Bitmap getThumbnailBitmap(final Context context) {
        return ImageCache.getBitmap(context, thumbnailUri);
    }

    public Bitmap getPhotoBitmap(final Context context) {
        return ImageCache.getBitmap(context, photoUri);
    }

    public boolean hasPhotoUri() {
        return photoUri != null && photoUri.trim().length() > 0;
    }

    public SpecialDateType getRepresentativeType() {
        if (types.size() > 0) {
            return types.get(0);
        } else {
            return null;
        }
    }

    public Notification createNotification(final Context context) {

        int uniqueIdentifier = -1;
        try {
            uniqueIdentifier = (Integer.parseInt(id) * 10) + types.size();
        } catch (NumberFormatException ex) {
            DefaultActivity.debugNotify(context, "Unique Identifier", "Failed to create unique identifier for notification.", ex);
        }

        SpecialDateType representative = getRepresentativeType();

        String share;
        String title = context.getString(R.string.notif_title, name, representative.getName());
        String subject = context.getString(R.string.notif_action_email_subject_other);
        String note = null;
        NotificationCompat.Style style = null;

        if (representative.isBirthday()) {
            share = context.getString(R.string.notif_action_share_birthday);
            note = context.getString(R.string.notif_note_birthday);
            subject = context.getString(R.string.notif_action_email_subject_birthday);
        } else if (representative.isAnniversary()) {
            share = context.getString(R.string.notif_action_share_anniversary);
            note = context.getString(R.string.notif_note_anniversary);
            subject = context.getString(R.string.notif_action_email_subject_anniversary);
        } else if (representative.isOther()) {
            note = context.getString(R.string.notif_note_other, name);
            share = context.getString(R.string.notif_action_share_other);
        } else {
            share = context.getString(R.string.notif_action_share_other);
        }

        if (types.size() > 1) {
            title = context.getString(R.string.notif_title_big, name);
            String reasons = "";
            String sep = "";
            for (SpecialDateType type : types) {
                reasons += sep + type.getName();
                sep = ", ";
            }
            note = context.getString(R.string.notif_note_big, types.size());
            style = new NotificationCompat.BigTextStyle()
                    .bigText(context.getString(R.string.notif_note_big_expanded, types.size(), reasons));
        }

        Intent snooze = new Intent(context, SnoozeReceiver.class);
        snooze.putExtra(SpecialDateReceiver.EXTRA_SPECIAL_DATE, this);

        Intent emailIntent = null;
        if (email.trim().length() > 0) {
            emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", email, null));
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
        }

        Intent smsIntent = null;
        if (phone.trim().length() > 0) {
            smsIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("sms", phone, null));
            smsIntent.putExtra("sms_body", subject);
        }

        NotificationCompat.Builder b = new NotificationCompat.Builder(context)
                .setSmallIcon(representative.getSmallNotificationIcon())
                .setContentTitle(title)
                .setWhen(0);

        Bitmap thumbnailBitmap = ImageCache.getBitmap(context, thumbnailUri);
        if (thumbnailBitmap != null) {
            b.setLargeIcon(thumbnailBitmap);
        }

        if (types.size() == 1) {
            if (representative.hasAge()) {
                b.setContentInfo(representative.getAge());
            }
        }

        if (style != null) {
            b.setStyle(style);
        }

        if (note != null) {
            b.setContentText(note);
        }

        List<Notification> pages = new ArrayList<Notification>();
        if (types.size() > 1) {
            for (SpecialDateType type : types) {
                pages.add(new SpecialDate(id, name, email, phone, type, thumbnailUri, photoUri).createNotification(context));
            }
        }

        // Wear Stuff
        NotificationCompat.WearableExtender extender = new NotificationCompat.WearableExtender()
                .addAction(new NotificationCompat.Action(android.R.drawable.ic_lock_idle_alarm, context.getString(R.string.notif_action_snooze), PendingIntent.getBroadcast(context, notificationId, snooze, PendingIntent.FLAG_ONE_SHOT)))
                .addAction(new NotificationCompat.Action(android.R.drawable.ic_dialog_email, context.getString(R.string.notif_action_share_email), PendingIntent.getActivity(context, notificationId, emailIntent, PendingIntent.FLAG_ONE_SHOT)))
//                .addAction(new NotificationCompat.Action(android.R.drawable.ic_dialog_sms, context.getString(R.string.notif_action_share_sms), PendingIntent.getActivity(context, notificationId, email, PendingIntent.FLAG_ONE_SHOT)))
                .addPages(pages);

        if (share != null) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            Uri uri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_URI, String.valueOf(id));
            intent.setData(uri);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            PendingIntent shareIntent = PendingIntent.getActivity(context, uniqueIdentifier, intent, PendingIntent.FLAG_ONE_SHOT);
            b.setContentIntent(shareIntent);
            b.addAction(android.R.drawable.ic_menu_share, share, shareIntent);

            extender.addAction(new NotificationCompat.Action(android.R.drawable.ic_menu_share, share, shareIntent));
        }
        b.addAction(android.R.drawable.ic_lock_idle_alarm, context.getString(R.string.notif_action_snooze), PendingIntent.getBroadcast(context, uniqueIdentifier, snooze, PendingIntent.FLAG_ONE_SHOT));

        return extender.extend(b).build();
    }


    public void setEmail(final String email) {
        this.email = email;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public static String zeroPad(final int num) {
        return num < 10 ? "0" + num : "" + num;
    }
}
