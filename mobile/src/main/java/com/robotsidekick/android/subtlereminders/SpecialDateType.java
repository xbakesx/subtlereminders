package com.robotsidekick.android.subtlereminders;

import android.content.Context;
import android.util.Log;

import java.io.Serializable;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by alex on 7/24/14.
 */
public class SpecialDateType implements Serializable {
    private static SimpleDateFormat formatWithYear = new SimpleDateFormat("MMMM d, yyyy");
    private static SimpleDateFormat formatWithoutYear = new SimpleDateFormat("MMMM d");
    private static Pattern datePattern = Pattern.compile("(\\d+|-)-(\\d+)-(\\d+)");
    private static Random rand = new Random();
    static {
        rand.setSeed(System.currentTimeMillis());
    }

    private SpecialDateType(final String iName, final String iDefaultName, final Type iType, final Integer iAge) {
        name = iName;
        defaultName = iDefaultName;
        type = iType;
        age = iAge;
    }

    public SpecialDateType(final Context context, final String iType, final String iName, String iDate) {
        int intType = 0;
        try {
            intType = Integer.parseInt(iType);
        } catch (NumberFormatException ex) {
            // treat as custom
        }
        type = Type.fromConfig(intType);
        if (isBirthday()) {
            name = context.getString(R.string.date_type_birthday);
        } else if (isAnniversary()) {
            name = context.getString(R.string.date_type_anniversary);
        } else {
            name = (iName == null || iName.trim().length() == 0 ? null : iName);
        }
        defaultName = context.getString(R.string.date_type_default);

        Calendar now = SpecialDate.getNow();
        Matcher m = datePattern.matcher(iDate);
        if (m.matches()) {
            try {
                int year = Integer.parseInt(m.group(1));
                age = (now.get(Calendar.YEAR) - year);
                date = new GregorianCalendar(year, Integer.parseInt(m.group(2)) - 1, Integer.parseInt(m.group(3)));
            } catch (NumberFormatException ex) {
                age = null;
                date = null;
            }

            int year = now.get(Calendar.YEAR);
            GregorianCalendar modifiedCalendar = new GregorianCalendar(year, Integer.parseInt(m.group(2)) - 1, Integer.parseInt(m.group(3)));
            if (modifiedCalendar.compareTo(now) < 0) {
                year += 1;
            }

            try {
                dateForComparator = new GregorianCalendar(year, modifiedCalendar.get(Calendar.MONTH), modifiedCalendar.get(Calendar.DAY_OF_MONTH));
            } catch (NumberFormatException ex2) {
                dateForComparator = null;
            }

        } else {
            age = null;
            date = null;
            dateForComparator = null;
        }
        dateString = iDate;
    }

    public static String toString(Calendar cal) {
        if (cal == null) {
            return "-null-";
        }
        return cal.get(Calendar.YEAR) + "/" + (cal.get(Calendar.MONTH) + 1) + "/" + cal.get(Calendar.DAY_OF_MONTH);
    }

    private String name;
    private final String defaultName;
    private Type type;
    private Integer age;
    private Calendar date;
    private Calendar dateForComparator;
    private String dateString;

    public boolean isBirthday() {
        return Type.BIRTHDAY.equals(type);
    }
    public boolean isAnniversary() {
        return Type.ANNIVERSARY.equals(type);
    }
    public boolean isCustom() {
        return !isBirthday() && !isAnniversary() && name != null;
    }
    public boolean isOther() {
        return !isBirthday() && !isAnniversary() && name == null;
    }
    public String getName() {
        return name == null ? defaultName : name;
    }
    public boolean hasAge() { return age != null; }
    public String getAge() { return age == null ? "" : age + ""; }
    public String getDateAsString() {
        String dateAsString;
        if (date != null) {
            dateAsString = formatWithYear.format(date.getTime());
        } else {
            dateAsString = formatWithoutYear.format(dateForComparator.getTime());
        }
        return dateAsString;
    }
    public String getUpcomingAge() {
        if (hasAge() && date != null) {
            return "" + (dateForComparator.get(Calendar.YEAR) - date.get(Calendar.YEAR));
        } else {
            return "";
        }
    }
    public int getUpcomingAgeAsInt() {
        if (hasAge() && date != null) {
            return (dateForComparator.get(Calendar.YEAR) - date.get(Calendar.YEAR));
        } else {
            throw new NumberFormatException("No Age or Date to calculate from is null");
        }
    }
    public Calendar getDate() { return date; }
    public Calendar getDateForComparator() { return dateForComparator; }
    public int getSmallNotificationIcon() {
        if (isBirthday()) {
            return rand.nextBoolean() ? R.drawable.ic_cake : R.drawable.ic_cupcake;
        } else if (isAnniversary()) {
            return R.drawable.ic_ring;
        } else {
            return android.R.drawable.ic_dialog_info;
        }
    }
    public String toString() {
        return getName();
    }

    public SpecialDateType copy() {
        return new SpecialDateType(this.name, this.defaultName, this.type, this.age);
    }

    private enum Type {
        CUSTOM(0),
        ANNIVERSARY(1),
        OTHER(2),
        BIRTHDAY(3);
        private int config;
        private Type(final int config) {
            this.config = config;
        }
        public static Type fromConfig(final int intType) {
            for (Type t : values()) {
                if (t.config == intType) {
                    return t;
                }
            }
            return CUSTOM;
        }
    }
}
