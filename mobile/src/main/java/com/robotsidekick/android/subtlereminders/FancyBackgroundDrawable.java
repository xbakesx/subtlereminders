package com.robotsidekick.android.subtlereminders;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.View;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * Created by alex on 1/5/15.
 */
public class FancyBackgroundDrawable extends Drawable {

    private static final String TAG = FancyBackgroundDrawable.class.getCanonicalName();
    private final Paint paint;
    private final Context context;
    private final String drawableUri;

    private final View view;

//    private float blur;

    public FancyBackgroundDrawable(final Context context, final String drawableUri, View view) {
        this.context = context;
        this.drawableUri = drawableUri;
        this.view = view;

        this.paint = new Paint();
        this.paint.setAntiAlias(true);

        setDesaturation(.3f);
//        setBlur(25f);
    }

    public void setDesaturation(float desaturation) {
        ColorMatrix desaturationMatrix = new ColorMatrix();
        desaturationMatrix.setSaturation(desaturation);
        paint.setColorFilter(new ColorMatrixColorFilter(desaturationMatrix));
    }

//    public void setBlur(final float blur) {
//        this.blur = blur;
//    }

    @Override
    public void draw(final Canvas canvas) {

        int width = canvas.getWidth();
        int height = canvas.getHeight();
        int larger;
        int smaller;

        if (width > height) {

            larger = width;
            smaller = height;

            Future<Bitmap> background = getBitmap(larger, larger);

            if (background.isDone()) {
                float top = (larger - smaller) / 2f;
                try {
                    canvas.drawBitmap(background.get(), 0f, -1 * top, paint);
                } catch (InterruptedException e) {
                    Log.e(TAG, "Failed to draw contact photo", e);
                } catch (ExecutionException e) {
                    Log.e(TAG, "Failed to draw contact photo", e);
                }
            }

        } else {

            larger = height;
            smaller = width;

            Future<Bitmap> background = getBitmap(larger, larger);
            if (background.isDone()) {
                float left = (larger - smaller) / 2f;
                try {
                    canvas.drawBitmap(background.get(), -1 * left, 0f, paint);
                } catch (InterruptedException e) {
                    Log.e(TAG, "Failed to draw contact photo", e);
                } catch (ExecutionException e) {
                    Log.e(TAG, "Failed to draw contact photo", e);
                }
            }
        }

    }

    private Future<Bitmap> getBitmap(final int width, final int height) {
        if (drawableUri == null || drawableUri.trim().length() == 0) {
            return null;
        }

        final String key = "fancy_" + width + "x" + height + "_" + drawableUri;

        return ImageCache.getBitmapLater(context, key, drawableUri, new ImageCache.BitmapTransformer() {
            @Override
            public Bitmap transform(final Context context, final Bitmap bitmap) {

                try {
                    return Bitmap.createScaledBitmap(bitmap, width, height, false);
                } catch (Throwable t) {
                    // Memory problems from VERY fast scrolling...
                    Log.e(TAG, "Failed to clean up memory from exceedingly fast scrolling", t);
                    return bitmap;
                }

                // To enable blur: (I couldn't get it to be responsive enough to use it in a list)
//                Bitmap finalBitmap = Bitmap.createScaledBitmap(bitmap, width, height, false);
//                Bitmap blurredBitmap = Bitmap.createScaledBitmap(finalBitmap, width, width, false);
//
//                final RenderScript rs = RenderScript.create(context);
//                final Allocation input = Allocation.createFromBitmap(rs, blurredBitmap, Allocation.MipmapControl.MIPMAP_NONE, Allocation.USAGE_SCRIPT);
//                final Allocation output = Allocation.createTyped(rs, input.getType());
//                final ScriptIntrinsicBlur script = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs));
//                script.setRadius(blur);
//                script.setInput(input);
//                script.forEach(output);
//                output.copyTo(blurredBitmap);
//
//                return blurredBitmap;
            }
        }, new ImageCache.BitmapLoadedCallback() {
            @Override
            public void complete(final Bitmap bitmap) {
                if (view != null) {
                    view.postInvalidate();
                }
            }
        });
    }

    @Override
    public void setAlpha(int alpha) {
        paint.setAlpha(alpha);
    }

    @Override
    public void setColorFilter(ColorFilter cf) {
        paint.setColorFilter(cf);
    }

    @Override
    public int getOpacity() {
        return PixelFormat.OPAQUE;
    }

}
