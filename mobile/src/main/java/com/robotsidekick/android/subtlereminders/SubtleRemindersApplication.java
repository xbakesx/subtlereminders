package com.robotsidekick.android.subtlereminders;

import android.app.Application;

/**
 * Created by alex on 12/31/14.
 *
 * Api level checks
 * Image Scale ratio on backgrounds (some aren't squares)
 *
 */
public class SubtleRemindersApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
    }
}
